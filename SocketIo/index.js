var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var timeOut = null;


app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});


io.on('connection', function(socket){
    console.log('a user connected');
    console.log(socket.id);

    socket.emit('getID', socket.id );

    // io.emit('chat message', 'a user connected');
    socket.broadcast.emit('chat message','a user connected');
    
    socket.on('disconnect', function(){
        console.log('user disconnected');
        io.emit('chat message', 'a user disconnected');
        // socket.broadcast.emit('a user disconnected');
    });

    socket.on('chat message', function(msg){
        socket.broadcast.emit('chat message', msg);
    });

    socket.on('someOneIsTypingServer', function(msg){
        console.log('message typing: ' + msg);
        socket.broadcast.emit('someOneIsTypingClient', msg);

        if( timeOut ){
            clearTimeout(timeOut);
        }

        timeOut = setTimeout(function(){
            socket.broadcast.emit('imDoneTypingClient', msg); 
            console.log("im done typing server");
        },500);

    });
    
});

http.listen(3000, function(){
    console.log('listening on *:3000');
});