var book = require ("./lib/grades").book;

var express = require("express");
var app = express();

app.get("/", function(req, res){

	res.send("Hello, World!");
});

app.get("/grade", function(req, res){

	var grades = req.query.grades.split(",");
	for(x in grades){
		book.addGrade(parseInt(grades[x]));
	}

	var average = book.getAverage();
	var letter = book.getLetterGrade();

	res.send("Your average is " + average + " grade " + letter);
});

app.listen(3000);
console.log("Server ready...");