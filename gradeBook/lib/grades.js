var gradeBook = {
	
	_grades: [],
	_total : 0,

	addGrade: function(newGrade){
		this._grades.push(newGrade);
	},

	getCountOfGrades:function(){
		return this._grades.length;
	},

	getAverage:function(){

		this._total = 0;

		for( x in this._grades ){
			this._total += this._grades[x];
		}

		return this._total / this._grades.length;
	},

	getLetterGrade:function(){
		var average = this.getAverage();

		if( average >= 90 ){
			return 'A';
		} else if( average >= 80 ){
			return 'B';
		} else if( average >= 70 ){
			return 'C'
		} else if( average >= 60 ){
			return 'D';
		} else {
			return 'F';
		}
	},

	reset: function(){
		this._grades = [];
	}

};

exports.book = gradeBook;