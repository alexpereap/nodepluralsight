import express from 'express';
import path from 'path';
import open from 'open';
import webpack from 'webpack';
import config from '../webpack.config.dev';

/* eslint-disable no-console */

const port = 3000;
const app = express();
const compiler = webpack(config);

app.use(require('webpack-dev-middleware')(compiler,{
  noInfo: true,
  publicPath:config.output.publicPath
}));

app.get('/',function(req,res){
  res.sendFile(path.join(__dirname, '../src/index.html'));
});

app.get('/users', function(req,res){
  // dummy data
  res.json([
    {"id": 1, "firstName":"Bob","lastName":"Smith","email":"bob@gmail.com"},
    {"id": 2, "firstName":"Bob1","lastName1":"Smith1","email1":"bob1@gmail.com"},
    {"id": 3, "firstName":"Bob2","lastName2":"Smith2","email2":"bob2@gmail.com"},
  ]);
});

app.listen(port,function(err){
  if(err){
    console.log(err);
  } else {
    open('http://localhost:' + port);
  }
});
