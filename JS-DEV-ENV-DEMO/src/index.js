import './index.css';

import {getUsers} from './api/userApi';

// populate users table via api call
getUsers().then(result => {
  let usersBody = "";

  result.forEach(user =>{
    usersBody += `<tr>
    <td><a href="#" data-id="${user.id}" class="deleteUser">Delete</td>
    <td>${user.id}</td>
    <td>${user.firstName}</td>
    <td>${user.lastName}</td>
    <td>${user.email}</td>`;
  });

  global.document.getElementById('users').innerHTML = usersBody;

});
