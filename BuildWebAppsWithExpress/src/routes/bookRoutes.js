var express = require('express');

var bookRouter = express.Router();

var router = function(nav){
    var books = [
        {
            title: 'Test title',
            genre: 'Test Genre',
            author: 'Test author',
            read: false
        },
        {
            title: 'Test title2',
            genre: 'Test Genre2',
            author: 'Test author2',
            read: false
        },
        {
            title: 'Test title3',
            genre: 'Test Genre3',
            author: 'Test author3',
            read: true
        }
    ];
    
    bookRouter.route('/')
        .get(function (req, res) {
            res.render('bookListView', {
                title: 'Books',
                nav: nav,
                books: books
            });
        });
    
    bookRouter.route('/:id')
        .get(function (req, res) {
            var id = req.params.id;
            res.render('bookView', {
                title: 'Books',
                nav:nav,
                book: books[id]
            });
        });


    return bookRouter;    
}



module.exports = router;